<?php
	// CURRENT FILE IS PART OF SELECTION PROCCESS 
	// COMPANY: ONLINE BERATUNG
	// AUTHOR: MARCELO TOAPANTA
	// DATE: 19/FEB/2020
	// PHP VERSION: 7.1.19
	// OBJECTIVE: Create a program that helps organize a schedule event. 

	// =========== START MODEL ===========
	class Theme {
	  // Properties
	  public $name;
	  public $duration;

	  // Methods
	  function set_name($name) {
	    $this->name = $name;
	  }
	  function get_name() {
	    return $this->name;
	  }

	  function set_duration($duration) {
	    $this->duration = $duration;
	  }
	  function get_duration() {
	    return $this->duration;
	  }
	}
	// =========== END MODEL ===========

	// =========== START VIEW ===========
	if (preg_match('/\.(?:png|jpg|jpeg|gif)$/', $_SERVER["REQUEST_URI"])) {
	    return false;    // serve the requested resource as-is.
	} else { 
	    echo "<h1>Welcome to schedule manager 1.0</h1>";
	    echo "<p>Please load your text file and send us to process it</p>";

	    echo '<div style="border-radius: 5px;background-color: #f2f2f2;padding: 20px; width: 50%; min-width: 300px">
	    			<form action="schedule.php" method="post" enctype="multipart/form-data">
  						<label for="file">Filename:</label>
  						<input type="file" name="file" id="file"><br>
  						<input type="submit" name="submit" value="Submit" style="width: 100%;background-color: #4CAF50;color: white;padding: 14px 20px;margin: 8px 0;border: none;border-radius: 4px;cursor: pointer;">
						</form>
						</div>';
			$themes = array();
	}

	if (isset($_FILES['file'])) {
	   $file = $_FILES['file']['tmp_name'];

	   $fh = fopen($file,'r');
			while ($line = fgets($fh)) {
			  // <... Do your work with the line ...>
			  $cols = explode(", ", $line);
			  $theme = new Theme();
			  $theme->set_name($cols[0]);
				$theme->set_duration($cols[1]);
				array_push($themes, $theme);

			}
			fclose($fh);
	   setSessions($themes);
	}

	// =========== END VIEW ===========

	// =========== START CONTROLLER LOGIC ===========
	function groupThemes($totalMinutes, $aux_themes)
	{
		$counterMinutes = 0; // Minutes to complete session
		$groupThemes = array();
		while ($counterMinutes < $totalMinutes){
			$selectCriteria = $totalMinutes; //Inicial value
			$index = 0;		
			$aux_themes = array_values($aux_themes);
			foreach ($aux_themes as $theme) {
				$duration  = intval($theme->duration);
				if ( $duration < $selectCriteria && ($counterMinutes + $duration) <= $totalMinutes ){
					array_push($groupThemes, $theme); // Add theme to new order
					unset($aux_themes[$index]); // Remove from aux_temes
					$selectCriteria = $duration;
					$counterMinutes = $counterMinutes + $duration;
					if ($counterMinutes >= $totalMinutes || count($aux_themes) == 0){
						$counterMinutes = $totalMinutes + 1;
					}
				}
				$index = $index + 1;
			}
		}
		return [$groupThemes, $aux_themes];
	}

	function setSessions($themes) {
		//Logic to order themes by sessions
		$firstSession = 180; // Total minutes first session
    $secondSession = 240; // Total minutes second session
    $aux_themes = $themes; // Copy array of obects
    $sessionCounter = 1; // Session Split (to get firsr session or second session)
    $themesCounter = 1; // Index of themes
    $topicCounter = 1; // Index of topics

    //1) Order themes by time asc
    usort($themes, 'orderCriteria');
    
    //2) Complete session with themes, condition =>  first session: 180 min, second session 240 min
    while (count($aux_themes) > 0){
		  if ($sessionCounter%2==0){
		  	// Group themes
		  	$themesGrouped =  groupThemes($secondSession, $aux_themes);
		  }else{
		  	// Group themes
		  	$themesGrouped =  groupThemes($firstSession, $aux_themes);	
		  	// Set time to start session
		  	$date = new DateTime("09:00"); 
		  	// Print Header
		  	printHeader($topicCounter);
		  	$topicCounter++;
		  }
		  // 3 Get themes gruped to complete session
    	$themesBySession = $themesGrouped[0]; 
    	usort($themesBySession, 'orderCriteria'); // Order by time asc (Opcional)
    	$aux_themes = $themesGrouped[1]; // Get themes avaiables
    	// 4 Print results
	    foreach ($themesBySession as $theme) {
	    	// Print Themes
	    	printRow($themesCounter, $date->format('h:i A'), $theme->name, $theme->duration.' min', '');
	    	modifyDate($date, intval($theme->duration).' minutes');
	    	$themesCounter++;
	    	// Print Lunch
	    	if ($date == new DateTime("12:00")){
	    		printRow('', $date->format('h:i A'), 'LUNCH', "60 min", "color: red;");
	    		modifyDate($date, '60 minutes');
	    	}
		  }
		  $sessionCounter++;
		  // Print Social Event
		  if ($sessionCounter%2!=0){
		  	if ($date < new DateTime("16:00")){
		  		$date = new DateTime("16:00");
		  	}
	    	printRow('', $date->format('h:i A'), 'SOCIAL EVENT', "", "color: green;");
				modifyDate($date, '60 minutes');
				printFooter();
			}
		}
	}
	// =========== END CONTROLLER LOGIC ===========
	
	// =========== START FUNCTIONS ===========
	function orderCriteria($a, $b)
	{
	  return $a->duration < $b->duration;
	}

	function printHeader($topicCounter)
	{
		echo '<label style="margin: 20px 0px; display: block;">Topic '.$topicCounter.'</label>';
		echo '<table border="1" style="border-collapse: collapse; width: 50%; display: block; padding: 20px;">';
		echo '<tr style="padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #4CAF50;color: white;"><th>Nro</th><th>Hora</th><th>Temática</th><th>Tiempo</th></tr>';
	}
	function printRow($col1, $col2, $col3, $col4, $style)
	{
	  echo('<tr style="'.$style.'"><td style="padding: 8px;">'.$col1.'</td><td style="padding: 8px; width: 70px;">'.$col2.'</td><td style="padding: 8px;">'.$col3.'</td><td style="padding: 8px;">'.$col4.'</td></tr>');
	}
	function printFooter()
	{
		echo '</table>';
	}

	function modifyDate($date, $modifier)
	{
	  return $date->modify($modifier);
	}
	// =========== END FUNCTIONS ===========
?>