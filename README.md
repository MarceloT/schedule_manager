Instructions to run test

1) Unzip file schedule_manager.zip: unzqip schedule_manager.zip
2) move to schedule_manager folder: cd schedule_manager
3) start php server: php -S 0.0.0.0:8001 schedule.php
4) open a browser and go to: http://localhost:8001/schedule.php
